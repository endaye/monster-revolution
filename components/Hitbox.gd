extends Area2D

class_name Hitbox

export (int) var damage := 1
export (Vector2) var rebound_vector := Vector2.ZERO
export (bool) var disabled := false setget set_disabled

func set_disabled(_disabled: bool) -> void:
	if disabled != _disabled:
		disabled = _disabled
		for child in get_children():
			if child is CollisionShape2D or child is CollisionPolygon2D:
				child.set_deferred("disabled", disabled)
