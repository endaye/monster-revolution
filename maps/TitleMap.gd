extends Node2D

onready var door = $YSort/Door
onready var orc = $YSort/Orc

func open_door() -> void:
	door.opened = true
	
	var orc_state_machine = orc.get_node("StateMachine")
	if orc_state_machine:
		# Switch to idle state first, so we know we'll switch to Move.
		orc_state_machine.change_state("Idle")
		# Attempt to walk to the door.
		orc_state_machine.change_state("Move", {
			'input_vector': (door.global_position - orc.global_position).normalized(),
		})
