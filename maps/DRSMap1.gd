extends Node2D

onready var start_door = $YSort/StartDoor
onready var door1 = $YSort/Door1
onready var door2 = $YSort/Door2
onready var door3 = $YSort/Door3
onready var door4 = $YSort/Door4
onready var door5 = $YSort/Door5
onready var spike_timer1 = $SpikeTimer1
onready var switch3 = $Objects/Switch3
onready var spike_group_2_activated = "a"
onready var floor_button1 = $Objects/FloorButton1
onready var floor_button2 = $Objects/FloorButton2
onready var floor_button3 = $Objects/FloorButton3
#onready var floor_button4 = $Objects/FloorButton4
onready var floor_button5 = $Objects/FloorButton5
onready var floor_button6 = $Objects/FloorButton6
onready var stay_message_collision_shape = $StayMessageArea/CollisionShape2D
onready var wizard4 = $YSort/Wizard4
onready var orc10 = $YSort/Orc10
onready var orc11 = $YSort/Orc11
onready var orc12 = $YSort/Orc12
onready var orc13 = $YSort/Orc13

var already_showed_mode_message := false

func _on_WinZone_body_entered(body: Node) -> void:
	var monster_count = $RescueZone.get_overlapping_bodies().size()
	if monster_count < 1:
		monster_count = 1
	GameState.win(monster_count - 1)

func _on_Switch1_switch_toggled(switch_position) -> void:
	get_tree().set_group("spike_group_1", "spikes_activated", !switch_position)
	get_tree().set_group("tutorial_room", "frozen", false)

func _on_Switch2_switch_toggled(switch_position) -> void:
	door2.opened = switch_position

	if !already_showed_mode_message:
		already_showed_mode_message = true
		yield(get_tree().create_timer(1.0), "timeout")
		var message = "REMINDER!\n"
		message += "You can give a general order to your followers:\n\n"
		message += "FOLLOW = follow you\n"
		message += "ATTACK = attack nearby wizards\n"
		message += "NONE = don't follow or attack\n\n"
		if OS.has_touchscreen_ui_hint():
			message += "Press the B button to change your order!"
		else:
			message += "Press SHIFT or TAB to change your order!"
		Utils.show_message(message)

func _on_Door2_door_toggled(opened) -> void:
	if opened:
		get_tree().set_group("orc_group_1", "captive", false)

func _on_Switch3_switch_toggled(switch_position) -> void:
	door3.opened = switch_position or floor_button2.pressed
	if switch_position == Switch.SwitchPosition.RIGHT:
		spike_timer1.start()
	else:
		spike_timer1.stop()
		get_tree().set_group("spike_group_2", "spikes_activated", false)

func _on_SpikeTimer1_timeout() -> void:
	get_tree().set_group("spike_group_2", "spikes_activated", false)
	
	if switch3.switch_position != Switch.SwitchPosition.RIGHT or floor_button2.pressed:
		return
	
	spike_group_2_activated = "a" if spike_group_2_activated == "b" else "b"
	yield(get_tree().create_timer(0.5), "timeout")
	get_tree().set_group("spike_group_2" + spike_group_2_activated, "spikes_activated", true)
	spike_timer1.start()

func _on_FloorButton1_button_toggled(pressed) -> void:
	if pressed:
		door1.opened = true

func _on_FloorButton2_button_toggled(pressed) -> void:
	if pressed:
		door3.opened = true
		spike_timer1.stop()
		get_tree().set_group("spike_group_2", "spikes_activated", false)

func _on_Switch4_switch_toggled(switch_position) -> void:
	get_tree().set_group("spikes_group_3", "spikes_activated", !switch_position)
	get_tree().set_group("orc_group_2", "captive", false)

func _on_FloorButton3_button_toggled(pressed) -> void:
	if pressed:
		door4.opened = true

#func _on_FloorButton4_button_toggled(pressed) -> void:
#	if pressed:
#		floor_button3.pressed = !pressed
#		door4.opened = false

func _on_PrisonRoom1ActivationArea_body_entered(body: Node) -> void:
	get_tree().set_group("prison_room_1", "frozen", false)

func _on_FloorButton5_button_toggled(pressed) -> void:
	if pressed:
		start_door.opened = true
		floor_button6.pressed = false

func _on_FloorButton6_button_toggled(pressed) -> void:
	if pressed:
		start_door.opened = false
		floor_button5.pressed = false

func _on_StayMessageArea_body_entered(body: Node) -> void:
	stay_message_collision_shape.set_deferred("disabled", true)
	
	var message = "HEY MONSTER!\n\n"
	message += "This might be a good time to lead your followers away\n"
	message += "using FOLLLOW and then switch to NONE\n\n"
	message += "(They tend to wander around a bit...)"
	Utils.show_message(message)

func _on_Switch5_switch_toggled(switch_position) -> void:
	door5.opened = switch_position

func _on_WizardRoomActivationArea1_body_entered(body: Node) -> void:
	get_tree().set_group("wizard_room", "frozen", false)

func _on_FloorButton7_button_toggled(pressed) -> void:
	if pressed:
		get_tree().set_group("spikes_group_4b", "spikes_activated", false)
		wizard4.frozen = false

func _on_FloorButton8_button_toggled(pressed) -> void:
	if pressed:
		get_tree().set_group("spikes_group_8", "spikes_activated", false)
		get_tree().set_group("maze_room", "frozen", false)
		get_tree().set_group("maze_room", "captive", false)

func _on_SpikeTimer2_timeout() -> void:
	get_tree().call_group("spikes_group_5", "toggle_spikes")

func _on_SpikeTimer3_timeout() -> void:
	get_tree().call_group("spikes_group_6", "toggle_spikes")

func _on_SpikeTimer4_timeout() -> void:
	get_tree().call_group("spikes_group_7", "toggle_spikes")

func _on_FloorButton_button_toggled(pressed) -> void:
	get_tree().set_group("spikes_group_9", "spikes_activated", false)
	orc13.captive = false
	orc13.frozen = false

func _on_FloorButton9_button_toggled(pressed) -> void:
	get_tree().set_group("spikes_group_10", "spikes_activated", false)
	orc12.captive = false
	orc12.frozen = false

func _on_FloorButton10_button_toggled(pressed) -> void:
	get_tree().set_group("spikes_group_11", "spikes_activated", false)
	orc11.captive = false
	orc11.frozen = false


func _on_FloorButton11_button_toggled(pressed) -> void:
	get_tree().set_group("spikes_group_12", "spikes_activated", false)
	orc10.captive = false
	orc10.frozen = false

func _on_GridRoomActivationArea_body_entered(body: Node) -> void:
	get_tree().set_group("grid_room", "frozen", false)

func _on_PrisonRoom2ActivationArea_body_entered(body: Node) -> void:
	get_tree().set_group("prison_room_2", "frozen", false)

func _on_Switch6_switch_toggled(switch_position) -> void:
	get_tree().set_group("spikes_group_13", "spikes_activated", !switch_position)
	get_tree().set_group("spikes_group_14", "spikes_activated", !switch_position)
	get_tree().set_group("spikes_group_15", "spikes_activated", !switch_position)
	get_tree().set_group("spikes_group_16", "spikes_activated", !switch_position)
	get_tree().set_group("spikes_group_17", "spikes_activated", !switch_position)
	if switch_position == Switch.SwitchPosition.RIGHT:
		get_tree().set_group("prison_room_2", "captive", false)

func _on_Switch7_switch_toggled(switch_position) -> void:
	if switch_position == Switch.SwitchPosition.RIGHT:
		get_tree().set_group("spikes_group_4", "spikes_activated", false)
		var message = "This switch had no apparent effect...\n"
		message += "Perhaps it opened something deeper in the dungeon?\n\n"
		message += "Will you backtrack and try to save more of your comrades?\n\n"
		message += "Or will leave with those you've already saved?"
		Utils.show_message(message)
