extends Node2D

onready var door = $YSort/Door
onready var lock_button = $Objects/LockButton
onready var unlock_button = $Objects/UnlockButton
onready var switch = $Objects/Switch
onready var switch2 = $Objects/Switch2
onready var door2 = $YSort/Door2
onready var exit_spikes_timer = $ExitSpikesTimer

var exit_spikes_activated = "1"

func _on_UnlockButton_button_toggled(pressed) -> void:
	if pressed:
		door.opened = true
		lock_button.pressed = false
		switch.switch_position = Switch.SwitchPosition.RIGHT

func _on_LockButton_button_toggled(pressed) -> void:
	if pressed:
		door.opened = false
		unlock_button.pressed = false
		switch.switch_position = Switch.SwitchPosition.LEFT

func _on_Switch_switch_toggled(switch_position) -> void:
	if switch_position == Switch.SwitchPosition.LEFT:
		door.opened = false
		unlock_button.pressed = false
		lock_button.pressed = true
	elif switch_position == Switch.SwitchPosition.RIGHT:
		door.opened = true
		unlock_button.pressed = true
		lock_button.pressed = false

func _on_Door_door_toggled(opened) -> void:
	# When the door is opened, the orcs are switched to no longer be
	# "captive" which changes their behavior. Captive orcs just wander
	# around, they won't FOLLOW or ATTACK. This is to prevent them from
	# reacting to you before you've let them go, which can look super weird.
	if opened:
		$YSort/Orc1.captive = false
		$YSort/Orc2.captive = false

func _on_SpikesTimer_timeout() -> void:
	for spikes in get_tree().get_nodes_in_group("spikes"):
		spikes.spikes_activated = !spikes.spikes_activated

func _on_ExitSpikesTimer_timeout() -> void:
	if switch2.switch_position == Switch.SwitchPosition.LEFT:
		get_tree().call_group("exit_spikes", "set_spikes_activated", false)
		exit_spikes_activated = "2" if exit_spikes_activated == "1" else "1"
		yield(get_tree().create_timer(0.5), "timeout")
		get_tree().call_group("exit_spikes_" + exit_spikes_activated, "set_spikes_activated", true)
		exit_spikes_timer.start()

func _on_Switch2_switch_toggled(switch_position) -> void:
	if switch_position == Switch.SwitchPosition.LEFT:
		exit_spikes_timer.start()
		door2.opened = false
	else:
		exit_spikes_timer.stop()
		get_tree().call_group("exit_spikes", "set_spikes_activated", false)
		door2.opened = true

func _on_WinZone_body_entered(body: Node) -> void:
	GameState.win(0)
