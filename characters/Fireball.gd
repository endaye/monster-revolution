extends "res://components/Hitbox.gd"

var speed := 250.0
var acceleration := 400.0

var vector := Vector2.ZERO
var direction := Vector2.ZERO

var use_particles := true

func _ready() -> void:
	# "Unix" is what we get on FRT. So, disable particles under Raspberry Pi.
	if OS.get_name() == "Unix":
		use_particles = false
	$TrailParticles.emitting = use_particles

func start(start_position, _direction: Vector2):
	global_position = start_position
	direction = _direction
	rebound_vector = direction * speed

func _physics_process(delta: float) -> void:
	if direction != Vector2.ZERO:
		vector = vector.move_toward(direction * speed, acceleration * delta)
		position += vector * delta

func _on_Fireball_body_or_area_entered(body: Node) -> void:
	direction = Vector2.ZERO
	$CollisionShape2D.set_deferred("disabled", true)
	
	$TrailParticles.emitting = false
	$ExplosionParticles.emitting = use_particles
	$Sprite.visible = false
	$ExplosionSound.play()
	
	# Restart timer so explosion has time to play
	$DestructionTimer.stop()
	$DestructionTimer.start()

func _on_DestructionTimer_timeout() -> void:
	queue_free()
