extends KinematicBody2D

enum Appearance {
	PLAIN = 0,
	MASKED = 1,
	PAINTED = 2,
}

var appearance_regions = [
	Rect2(370, 206, 128, 18),
	Rect2(370, 174, 128, 18),
	Rect2(370, 239, 128, 17),
]

export (Appearance) var orc_appearance := Appearance.PLAIN setget set_orc_appearance
export (bool) var player_controlled := false setget set_player_controlled
export (float) var speed := 50.0
export (float) var bump_speed := 200.0
export (float) var rebound_speed := 25.0
export (bool) var captive := true
export (bool) var frozen := false
export (bool) var invincible := false

onready var initial_scale = scale
onready var sprite := $Sprite
onready var animation_player := $AnimationPlayer
onready var hitbox := $Hitbox
onready var hurtbox := $Hurtbox
onready var player_detection_area := $PlayerDetectionArea
onready var enemy_detection_area := $EnemyDetectionArea
onready var state_machine := $StateMachine
onready var state_debug_label := $StateDebugLabel
onready var health := $Health

onready var attack_sound = $AttackSound
onready var hurt_sounds = $HurtSounds

var direction := Vector2.RIGHT setget set_direction

func _ready() -> void:
	state_machine.change_state("Idle")

func set_orc_appearance(_orc_appearance: int) -> void:
	if orc_appearance != _orc_appearance:
		orc_appearance = _orc_appearance
		$Sprite.region_rect = appearance_regions[orc_appearance]

func set_player_controlled(_player_controlled: bool) -> void:
	if player_controlled != _player_controlled:
		player_controlled = _player_controlled
		$PlayerDetectionArea/CollisionShape2D.set_deferred("disabled", player_controlled)
		$EnemyDetectionArea/CollisionShape2D.set_deferred("disabled", player_controlled)
		set_collision_layer_bit(5, player_controlled)

func set_direction(_direction: Vector2) -> void:
	if direction != _direction:
		direction = _direction
	if direction.x < 0:
		scale.x = -initial_scale.x * sign(scale.y)
	else:
		scale.x = initial_scale.x * sign(scale.y)

func move_and_slide_with_push(linear_velocity):
	var delta = get_physics_process_delta_time()
	var collision = move_and_collide(linear_velocity * delta)
	if collision:
		var collider = collision.collider
		if collider is KinematicBody2D and collider.get_collision_layer_bit(1):
			# If we collide with a monster, then move that monester at 50% speed.
			collider.move_and_slide(collision.remainder * (linear_velocity.length() * 0.5))
		else:
			var motion = collision.remainder.slide(collision.normal)
			move_and_collide(motion)
	
func _physics_process(delta: float) -> void:
	var fv = get_floor_velocity()
	if fv != Vector2.ZERO:
		print (fv)

func _on_Hurtbox_area_entered(area: Area2D) -> void:
	var state = state_machine.current_state.name
	if state == 'Hurt' or state == 'Dead':
		# Don't process getting hurt if we are already hurt (and so invincible
		# for a little bit) or already dead.
		return
	
	if !invincible:
		var damage = area.get("damage")
		if damage == null:
			damage = 1
		health.health -= damage
	
	var rebound_vector = area.get("rebound_vector")
	if !rebound_vector or rebound_vector == Vector2.ZERO:
		rebound_vector = direction * -200
	
	state_machine.change_state("Hurt", {"rebound_vector": rebound_vector})

func _on_StateMachine_state_changed(state) -> void:
	state_debug_label.text = state.name
