extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."
onready var attack_timer = $AttackTimer

var vector := Vector2.ZERO

func _state_enter(info: Dictionary) -> void:
	var input_vector = info.get("input_vector", Vector2.ZERO)
	if input_vector != Vector2.ZERO:
		host.direction = input_vector
	vector = input_vector * host.bump_speed
	host.hitbox.rebound_vector = vector
	host.hitbox.disabled = false
	host.sprite.material.set_shader_param("white", true)
	host.attack_sound.play()
	attack_timer.start()

func _state_exit() -> void:
	attack_timer.stop()
	host.hitbox.disabled = true
	host.sprite.material.set_shader_param("white", false)

func _physics_process(delta: float) -> void:
	host.move_and_slide_with_push(vector)

func _on_AttackTimer_timeout() -> void:
	if get_parent().current_state != self:
		return
	
	get_parent().change_state("Cooldown")
	
