extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

var vector := Vector2.ZERO

func _state_enter(info: Dictionary) -> void:
	if info.has("rebound_vector"):
		vector = info["rebound_vector"]
	
	host.animation_player.play("Hurt")
	
	var hurt_sounds = host.hurt_sounds.get_children()
	hurt_sounds[randi() % hurt_sounds.size()].play()
	
	host.hurtbox.disabled = true

func _state_exit() -> void:
	host.animation_player.stop()
	host.modulate = Color(1.0, 1.0, 1.0, 1.0)
	host.hurtbox.disabled = false

func _physics_process(delta: float) -> void:
	host.move_and_slide(vector)

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if get_parent().current_state != self:
		return
	
	if anim_name == "Hurt":
		if host.health.health == 0:
			get_parent().change_state("Dead")
		else:
			get_parent().change_state("Idle")
