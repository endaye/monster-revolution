extends "res://addons/snopek_state_machine/State.gd"

onready var host := $"../.."
onready var idle_timer = $IdleTimer

var max_player_distance := 50

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Idle")
	
	if !host.player_controlled and idle_timer:
		idle_timer.wait_time = (randf() * 2.0) + 0.5
		idle_timer.start()

func _state_exit() -> void:
	if idle_timer:
		idle_timer.stop()

func _get_player_input_vector() -> Vector2:
	return Vector2(
		Input.get_action_strength("player_right") - Input.get_action_strength("player_left"),
		Input.get_action_strength("player_down") - Input.get_action_strength("player_up")
	).normalized()

func _check_for_player_attack_input(input_vector: Vector2) -> bool:
	if Input.is_action_just_pressed("player_action"):
		var attack_vector = input_vector if input_vector != Vector2.ZERO else host.direction
		get_parent().change_state("Attack", {"input_vector": attack_vector})
		return true
	return false

func _detect_player():
	if host.captive or host.frozen or GameState.minion_mode == GameState.MinionMode.STAY:
		return null
	
	var bodies = host.player_detection_area.get_overlapping_bodies()
	if bodies.size() > 0:
		if bodies[0].global_position.distance_to(host.global_position) > max_player_distance:
			return bodies[0]
	
	return null

func _detect_enemy():
	if host.captive or host.frozen or GameState.minion_mode == GameState.MinionMode.STAY:
		return null
	
	var bodies = host.enemy_detection_area.get_overlapping_bodies()
	if bodies.size() > 0:
		return Utils.get_closest_body(host.global_position, bodies)
	
	return null

func _physics_process(delta: float) -> void:
	if host.player_controlled:
		var input_vector = _get_player_input_vector()
		if !_check_for_player_attack_input(input_vector) and input_vector != Vector2.ZERO:
			get_parent().change_state("Move", {
				"input_vector": input_vector, 
				"immediately": true,
			})
	else:
		if GameState.minion_mode == GameState.MinionMode.ATTACK:
			var enemy = _detect_enemy()
			if enemy:
				get_parent().change_state("Move", {
					"input_vector": (enemy.global_position - host.global_position).normalized(),
					"immediately": true,
				})
				return
		
		var player = _detect_player()
		if player:
			get_parent().change_state("Move", {
				"input_vector": (player.global_position - host.global_position).normalized(),
				"immediately": true,
			})
			return


func _on_IdleTimer_timeout() -> void:
	if host.frozen:
		return
	if get_parent().current_state != self:
		return
	
	var input_vector = Vector2(randf() - 0.5, randf() - 0.5).normalized()
	get_parent().change_state("Move", {"input_vector": input_vector})
