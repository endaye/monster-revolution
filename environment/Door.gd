tool
extends StaticBody2D

class_name Door

export (bool) var opened := false setget set_opened

signal door_toggled (opened)

func set_opened(_opened: bool) -> void:
	if opened != _opened:
		opened = _opened
		if opened:
			$DoorClosedSprite.visible = false
			$DoorCollisionShape.set_deferred("disabled", true)
			$DoorOpenSound.play()
		else:
			$DoorClosedSprite.visible = true
			$DoorCollisionShape.set_deferred("disabled", false)
		emit_signal("door_toggled", opened)
