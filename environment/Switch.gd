tool
extends StaticBody2D

class_name Switch

enum SwitchPosition {
	LEFT = 0,
	RIGHT = 1,
}

export (SwitchPosition) var switch_position := SwitchPosition.LEFT setget set_switch_position

signal switch_toggled (switch_position)

func set_switch_position(_switch_position: int) -> void:
	if switch_position != _switch_position:
		switch_position = _switch_position
		$Sprite.frame = switch_position
		$SwitchSound.play()
		emit_signal("switch_toggled", switch_position)
		
		if get_tree():
			$Sprite.material.set_shader_param("white", true)
			yield(get_tree().create_timer(0.1), "timeout")
			$Sprite.material.set_shader_param("white", false)

func _on_Hurtbox_area_entered(area: Area2D) -> void:
	set_switch_position(!switch_position)
