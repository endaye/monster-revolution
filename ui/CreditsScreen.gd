extends Control

signal back

func show_screen(info: Dictionary) -> void:
	$Button.grab_focus()

func _on_Button_pressed() -> void:
	emit_signal("back")
