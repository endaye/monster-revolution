extends TextureRect

export (String) var player_up_action = 'player_up'
export (String) var player_down_action = 'player_down'
export (String) var player_left_action = 'player_left'
export (String) var player_right_action = 'player_right'
export (float) var dead_zone := 0.2

var pressed: int = -1
var pressed_position := Vector2.ZERO

func has_point(point: Vector2) -> bool:
	var center_pos = rect_global_position + (rect_size / 2.0)
	if center_pos.distance_to(point) <= (rect_size.x / 2.0):
		return true
	return false

func _send_input_action(action: String, strength: float) -> void:
	var event = InputEventAction.new()
	event.action = action
	event.pressed = true if strength > dead_zone else false
	event.strength = strength
	Input.parse_input_event(event)

func _send_all_events() -> void:
	var scaled_pressed_position: Vector2 = Vector2.ZERO
	
	if pressed != -1:
		scaled_pressed_position = ((pressed_position - Vector2(0.5, 0.5)) * 2.0).normalized()
	
	if scaled_pressed_position.y < -dead_zone:
		_send_input_action(player_up_action, abs(scaled_pressed_position.y))
	else:
		_send_input_action(player_up_action, 0.0)
	
	if scaled_pressed_position.y > dead_zone:
		_send_input_action(player_down_action, scaled_pressed_position.y)
	else:
		_send_input_action(player_down_action, 0.0)
	
	if scaled_pressed_position.x < -dead_zone:
		_send_input_action(player_left_action, abs(scaled_pressed_position.x))
	else:
		_send_input_action(player_left_action, 0.0)
		
	if scaled_pressed_position.x > dead_zone:
		_send_input_action(player_right_action, scaled_pressed_position.x)
	else:
		_send_input_action(player_right_action, 0.0)

func set_pressed_position_from_global_position(global_position: Vector2) -> void:
	pressed_position = (global_position - rect_global_position) / rect_size
	_send_all_events()

func _input(event) -> void:
	if event is InputEventScreenTouch:
		if pressed == -1 and event.pressed and has_point(event.position):
			pressed = event.index
			set_pressed_position_from_global_position(event.position)
			material.set_shader_param("enabled", true)
		elif not event.pressed and event.index == pressed:
			pressed = -1
			_send_all_events()
			material.set_shader_param("enabled", false)
	elif event is InputEventScreenDrag and event.index == pressed:
		set_pressed_position_from_global_position(event.position)

func _process(delta: float) -> void:
	if pressed != -1:
		material.set_shader_param("position", pressed_position)
