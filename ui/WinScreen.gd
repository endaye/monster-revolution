extends Control

onready var continue_button = $HBoxContainer/ContinueButton

signal continue_pressed

func show_screen(info: Dictionary) -> void:
	var monster_count = info['monster_count']
	if monster_count == 0:
		$Text2.text = '... but you failed to rescue any other monsters'
	else:
		$Text2.text = '... and rescued ' + str(monster_count) + ' monsters!'
	$HBoxContainer/ContinueButton.grab_focus()

func _on_ContinueButton_pressed() -> void:
	emit_signal("continue_pressed")
