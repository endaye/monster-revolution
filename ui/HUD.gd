extends Control

onready var health_ui := $HealthUI
onready var mode_label := $Mode/ModeLabel
onready var mode_sprite := $Mode/ModeSprite

signal mode_changed

func _ready() -> void:
	GameState.connect("player_set", self, "_on_GameState_player_set")
	GameState.connect("player_removed", self, "_on_GameState_player_removed")
	GameState.connect("minion_mode_changed", self, "_on_GameState_minion_mode_changed")
	
	_on_GameState_minion_mode_changed(GameState.minion_mode)

func _physics_process(delta: float) -> void:
	if visible and Input.is_action_just_pressed("player_toggle_mode"):
		var minion_mode = GameState.minion_mode
		minion_mode += 1
		if minion_mode >= GameState.MinionMode.MAX:
			minion_mode = 0
		GameState.minion_mode = minion_mode
		emit_signal("mode_changed")

func _on_GameState_player_set(player) -> void:
	health_ui.max_health = player.health.max_health
	health_ui.health = player.health.health
	player.health.connect("max_health_changed", health_ui, "set_max_health")
	player.health.connect("health_changed", health_ui, "set_health")

func _on_GameState_player_removed(player) -> void:
	player.health.disconnect("max_health_changed", health_ui, "set_max_health")
	player.health.disconnect("health_changed", health_ui, "set_health")

func _on_GameState_minion_mode_changed(minion_mode) -> void:
	if minion_mode == GameState.MinionMode.FOLLOW:
		mode_label.text = 'FOLLOW'
		mode_sprite.frame = 0
	elif minion_mode == GameState.MinionMode.ATTACK:
		mode_label.text = 'ATTACK'
		mode_sprite.frame = 1
	elif minion_mode == GameState.MinionMode.STAY:
		mode_label.text = 'NONE'
		mode_sprite.frame = 2
