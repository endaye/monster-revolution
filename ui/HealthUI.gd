extends Control

export (int) var max_health := 6 setget set_max_health
export (int) var health := 3 setget set_health

const HEART_LIMIT := 10

onready var tilemap = $TileMap
onready var empty_heart_tile: int = tilemap.tile_set.find_tile_by_name("EmptyHeart")
onready var half_heart_tile: int = tilemap.tile_set.find_tile_by_name("HalfHeart")
onready var full_heart_tile: int = tilemap.tile_set.find_tile_by_name("FullHeart")

func _ready() -> void:
	update_tilemap()

func set_max_health(_max_health: int) -> void:
	if max_health != _max_health:
		max_health = clamp(_max_health, 1, HEART_LIMIT * 2)
		health = clamp(health, 0, max_health)
		update_tilemap()

func set_health(_health: int) -> void:
	if health != _health:
		health = clamp(_health, 0, max_health)
		update_tilemap()

func update_tilemap() -> void:
	if !tilemap:
		return
	
	var tile: int
	
	for x in range(0, HEART_LIMIT):
		var x_health: int = (x + 1) * 2
		if x_health <= health:
			tile = full_heart_tile
		elif x_health > health and x_health == health + 1:
			tile = half_heart_tile
		elif x_health <= max_health + (max_health % 2):
			tile = empty_heart_tile
		else:
			tile = -1
		
		tilemap.set_cell(x, 0, tile)
