tool
extends Node

export (String, MULTILINE) var allowed_transitions setget set_allowed_transitions

var current_state
var allowed_transitions_parsed := {}

signal state_changed (state)

# @todo Causes some error in game!
#func _get_configuration_warning() -> String:
#	var bad_children = PoolStringArray()
#	for child in get_children():
#		if not child is preload("res://addons/snopek_state_machine/State.gd"):
#			bad_children.append(child.name)
#	if bad_children.size() > 0:
#		return "All direct children of StateMachine must be State: " + bad_children.join(", ")
#	return ""

func set_allowed_transitions(_allowed_transitions) -> void:
	if allowed_transitions != _allowed_transitions:
		allowed_transitions = _allowed_transitions
		
		allowed_transitions_parsed = {}
		for line in allowed_transitions.split("\n", false):
			var parts = line.split("->", false)
			if parts.size() == 2:
				var start = parts[0].strip_edges()
				var end = parts[1].strip_edges()
				
				if !allowed_transitions_parsed.has(start):
					allowed_transitions_parsed[start] = []
				allowed_transitions_parsed[start].append(end)

func _ready():
	_disable_children()

func _disable_children() -> void:
	for child in get_children():
		child.set_process_input(false)
		child.set_process_unhandled_input(false)
		child.set_process_unhandled_key_input(false)
		child.set_process(false)
		child.set_physics_process(false)

func check_allowed_transition(start, end) -> bool:
	if allowed_transitions_parsed.size() == 0:
		return true
	if !allowed_transitions_parsed.has(start):
		return false
	return allowed_transitions_parsed[start].find(end) != -1

func change_state(name : String, info : Dictionary = {}):
	var next_state = get_node(name)
	if next_state == null:
		return
	
	if current_state == next_state:
		return
	
	if current_state:
		if !check_allowed_transition(current_state.name, next_state.name):
			return

	if current_state:
		if current_state.has_method('_state_exit'):
			current_state._state_exit()
		
	_disable_children()
	
	var previous_state = current_state
	current_state = next_state
	
	# Re-enable processing for the current state
	if current_state.has_method('_input'):
		current_state.set_process_input(true)
	if current_state.has_method('_unhandled_input'):
		current_state.set_process_unhandled_input(true)
	if current_state.has_method('_unhandled_key_input'):
		current_state.set_process_unhandled_key_input(true)
	if current_state.has_method('_process'):
		current_state.set_process(true)
	if current_state.has_method('_physics_process'):
		current_state.set_physics_process(true)	
	
	if current_state != previous_state:
		if current_state.has_method('_state_enter'):
			current_state._state_enter(info)
	
	emit_signal("state_changed", current_state)

